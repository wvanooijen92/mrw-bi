# Quickstart
1. `cd /Project/Path`

2. `git clone https://[BITBUCKETURL]/wvanooijen92/jupyter.git .`

3. `pip install pipenv`

4. `pipenv run jupyter notebook`

# Dependencies
- Dit project heeft dependencies (software van anderen) om te werken (zoals bijvoorbeeld pymongo om met mongodb te connecten). In python kun je dependencies installeren met pip. 
- Het probleem met pip is dat deze dependencies globaal installeert, dit is vaak niet nodig om dat dependencies project specifiek zijn. Pipenv is een tool die project-specifieke dependencies managet.
- Ik heb pipenv geïnstalleerd, dit moet je ook doen om dit project te draaien (pip install pipenv) Het is belangrijk dat je jupiter met pipenv start, dus `pipenv run jupyter notebook` en dat als je een nieuwe dependancy installeert dat je `pipenv install pakkage-name` gebruikt (en dus niet `pip install package-name`).

# Custom Modules
- Om het iets makkelijker te maken om met de database te werken heb ik twee modules gemaakt. De eerste (settings.py) leest een bestand (.env) uit en maakt daar settings van. En een die een mongodb connectie opzet welke je kunt gebruiken in je notebook.