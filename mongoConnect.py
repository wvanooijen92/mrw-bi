#We will need to connect to mongo

def mongo():
	import os
	import urllib.parse
	import pymongo
	from pymongo import MongoClient

	print('Connecting to MongoDB...')
	DB_HOST = os.environ.get("DB_HOST", None)
	DB_USER = os.environ.get("DB_USER", None)
	DB_PASS = os.environ.get("DB_PASS", None)

	if None == DB_HOST or None == DB_USER or None == DB_PASS:
		print('DB_HOST / DB_USER / DB_PASS is not set in .env!')

	DB_USER = urllib.parse.quote(DB_USER)
	DB_PASS = urllib.parse.quote(DB_PASS)

	print('Mongo version', pymongo.__version__)
	MONGO_URL = "mongodb://" + DB_USER + ":" + DB_PASS + "@" + DB_HOST
	print('Connecting to mongo...')
	client = MongoClient(MONGO_URL)
	print('Connected!')
	return client